from fastapi import FastAPI
from httpx import AsyncClient

from common.utils import execute_with_timeout

app = FastAPI()


@execute_with_timeout(5)
async def _get_controller_data():
    async with AsyncClient() as client:
        response = await client.get('http://controller_api:8000/last_decision')
    return response.json()


@app.get("/get_controller_data")
async def get_controller_data():
    return await _get_controller_data() or {
        'message': 'controller is unavailable'
    }
