import os


CONTROLLING_TICK = int(os.environ.get('CONTROLLING_TICK', '5'))
AMQP_CREDS = {
    'login': 'guest',
    'password': 'guest',
    'host': 'rabbit',
}