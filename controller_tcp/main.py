import asyncio
import aioredis
import logging
import orjson
import socket

from aio_pika import connect_robust
from BTrees.OOBTree import OOBTree

from common.utils import formated_now, TCP, moved_datetime_string
import settings


class Worker():
    @classmethod
    def make_decision(cls, value):
        return 'up' if value %2 == 1 else 'down'

    async def _prepare(self):
        self.redis = await aioredis.create_redis_pool('redis://redis')
        self.amqp_connection = await connect_robust(
            'amqp://{login}:{password}@{host}/'.format(**settings.AMQP_CREDS),
            loop=asyncio.get_running_loop()
        )
        self.amqp_channel = await self.amqp_connection.channel()
        self.amqp_queue = await self.amqp_channel.declare_queue(
            'sensor_data',
            auto_delete=True
        )

        self.tcp = TCP()
        await self.tcp.connect()

        self.tree = OOBTree()

    async def _tear_down(self):
        await tcp.disconnect()
        await self.amqp_channel.close()
        await self.amqp_connection.close()
        self.redis.close()
        await self.redis.wait_closed()
    
    async def produce(self):
        async for message in self.amqp_queue:
            with message.process():
                data = orjson.loads(message.body)

                stored = self.tree.get(data['datetime'], 0)
                self.tree.update({data['datetime']: stored + data['payload']})
    
    async def consume(self):
        value = sum(self.tree.values(
            # берем только данные, у которых ключ
            # больше чем now() - 5 секунд
            moved_datetime_string(
                formated_now(),
                -1 * settings.CONTROLLING_TICK
            )
        ))
        self.tree.clear()

        decision = orjson.dumps({
            'datetime': formated_now(),
            'status': self.make_decision(value),
        })
        await self.tcp.send(decision)
        await self.redis.set('decision', decision)

        print(value)

    async def consume_infinite(self):
        while True:
            await asyncio.gather(
                asyncio.sleep(settings.CONTROLLING_TICK),
                self.consume()
            )
    
    @classmethod
    def fail_loudly(cls, future):
        if future.exception():
            future.result()

    @classmethod
    async def main(cls):
        worker = cls()
        await worker._prepare()

        tasks = [
            asyncio.create_task(worker.produce()),
            asyncio.create_task(worker.consume_infinite()),
        ]

        for task in tasks:
            task.add_done_callback(cls.fail_loudly)

        await asyncio.gather(*tasks, return_exceptions=True)

        await worker._tear_down()


if __name__ == "__main__":
    asyncio.run(Worker.main())
