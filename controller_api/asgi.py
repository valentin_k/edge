import asyncio
import orjson
import logging

import aio_pika
import aioredis
from fastapi import FastAPI, Request

from common.utils import formated_now, handle_exceptions, TCP, expiration_datetime
from schemas import SensorSchema
import settings


app = FastAPI()
queue = asyncio.Queue()


@app.on_event('startup')
async def startup():
    app.redis = await aioredis.create_redis_pool('redis://redis')
    app.amqp_connection = await aio_pika.connect_robust(
        'amqp://{login}:{password}@{host}/'.format(**settings.AMQP_CREDS),
        loop=asyncio.get_running_loop()
    )
    app.amqp_channel = await app.amqp_connection.channel()


@app.on_event('shutdown')
async def shutdown():
    app.redis.close()
    await app.redis.wait_closed()
    await app.amqp_channel.close()
    await app.amqp_connection.close()


@app.post('/sensor')
async def sensor(
    sensor_data: SensorSchema
):
    await app.amqp_channel.default_exchange.publish(
        aio_pika.Message(
            body=orjson.dumps(sensor_data.dict()),
            expiration=expiration_datetime(sensor_data.datetime, settings.CONTROLLING_TICK)
        ),
        routing_key='sensor_data'
    )
    return {'message': 'accepted'}


@app.get('/last_decision')
async def last_decision():
    decision = await app.redis.get('decision')
    if decision is None:
        return {'message': 'decision not yet ready'}
    
    decision = orjson.loads(decision)

    return {
        'datetime': formated_now(),
        'status': decision['status'],
        'time_of_decision': decision['datetime']
    }
