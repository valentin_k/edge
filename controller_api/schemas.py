from typing import Optional
from pydantic import PositiveFloat

from pydantic import UUID4, BaseModel, validator


class SensorSchema(BaseModel):
    payload: int
    datetime: str
