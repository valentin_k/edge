import os


COOLDOWN = float(os.environ.get('COOLDOWN', '5'))
AMOUNT_OF_MESSAGES = int(os.environ.get('AMOUNT_OF_MESSAGES', '3'))
CONTROLLER_URI = 'http://controller_api:8000/sensor'
