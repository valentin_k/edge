import asyncio
import logging
from random import random

import aiohttp

import settings
from common.utils import formated_now, handle_exceptions, execute_with_timeout


@handle_exceptions
@execute_with_timeout(settings.COOLDOWN)
async def make_request(session):
    async with session.post(
        settings.CONTROLLER_URI,
        headers={'content-type': 'application/json'},
        json={
            'datetime': formated_now(),
            'payload': 1,
        },
    ) as resp:
        if resp.status != 200:
            logging.warning(f'controller responded with {resp.status}')


async def main():
    await asyncio.sleep(3 + 2 * random())
    
    async with aiohttp.ClientSession() as session:
        while True:
            await asyncio.gather(
                asyncio.sleep(settings.COOLDOWN),
                *[
                    make_request(session)
                    for i in range(settings.AMOUNT_OF_MESSAGES)
                ]
            )
            logging.info('phase done')


if __name__ == "__main__":
    asyncio.run(main())
