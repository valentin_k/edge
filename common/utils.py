import asyncio
from datetime import datetime, timedelta
import logging
from functools import wraps


DATETIME_TEMPLATE = '%Y%m%dT%H:%M:%S'
# решил несколько улучшить предложенный формат дейттайма,
# включив туда секунды и разделители


def formated_dt(dt):
    return dt.strftime(DATETIME_TEMPLATE)


def formated_now():
    return formated_dt(datetime.now())


def expiration_datetime(datetime_string, expiration):
    dt = datetime.strptime(datetime_string, DATETIME_TEMPLATE)
    return dt + timedelta(seconds=5)


def moved_datetime_string(datetime_string, seconds_delta):
    dt = datetime.strptime(datetime_string, DATETIME_TEMPLATE)
    dt += timedelta(seconds=seconds_delta)
    return dt.strftime(DATETIME_TEMPLATE)


def execute_with_timeout(seconds):
    def decorator(function):
        @wraps(function)
        async def inner(*args, **kwargs):
            try:
                returned_value = await asyncio.wait_for(
                    function(*args, **kwargs),
                    timeout=seconds
                )
            except asyncio.TimeoutError:
                logging.warning(f'timeout: {function.__name__}')
                returned_value = None

            return returned_value
        return inner

    return decorator


def handle_exceptions(function):
    @wraps(function)
    async def inner(*args, **kwargs):
        try:
            returned_value = await function(*args, **kwargs)
        except Exception as e:
            logging.exception(e)
            returned_value = None

        return returned_value
    return inner

class TCP():
    def __init__(self):
        self.is_active = False

    @handle_exceptions
    async def connect(self):
        self.reader, self.writer = await asyncio.open_connection(
            'manipulator',
            8888
        )
        self.is_active = True

    @handle_exceptions
    async def disconnect(self):
        self.writer.close()
        await self.writer.wait_closed()
    
    async def send(self, data):
        if not self.is_active:
            await self.connect()
        if not self.is_active:
            return

        try:
            self.writer.write(data)
        except Exception as e:
            logging.exception(e)
            await self.disconnect()
            self.is_active = False
