import logging
import json
import socket


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    print('manipulator started')
    s.bind(('0.0.0.0', 8888))
    s.listen()
    conn, addr = s.accept()
    with conn:
        while True:
            data = conn.recv(1024)
            if not data:
                break
            value = json.loads(data.decode())
            print(f'Received: {value}')
